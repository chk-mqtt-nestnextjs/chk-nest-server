FROM node:16-buster-slim as compile-image

ENV NODE_ENV build

USER root
WORKDIR /usr/src/app

COPY . /usr/src/app
RUN npm install -g rimraf
RUN npm install -g @nestjs/cli 
RUN npm install

COPY . ./

RUN npm run build

FROM node:16-buster-slim

ENV NODE_ENV production

USER root
WORKDIR /usr/src/app

COPY --from=compile-image /usr/src/app/package*.json /usr/src/app/
COPY --from=compile-image /usr/src/app/dist/ /usr/src/app/dist/
COPY --from=compile-image /usr/src/app/uploads/ /usr/src/app/uploads/

RUN npm install

EXPOSE 3000
CMD ["node", "dist/src/main.js"]