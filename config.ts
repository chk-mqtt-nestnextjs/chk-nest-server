export default () => ({
    host: process.env.HOST,
    port: Number(process.env.PORT),
    database: process.env.DATABASE,
    username: process.env.USERNAME,
    password: process.env.PASSWORD
})