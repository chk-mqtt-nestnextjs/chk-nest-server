import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { LoggerOptions, ConnectionOptions } from 'typeorm';

export default class TypeOrmConfig {
  static getOrmConfig(configService: ConfigService): TypeOrmModuleOptions {
    console.log('configService ', configService);
    return {
      type: 'mysql',
      host: configService.get('host'),
      port: 3306,
      username: 'root',
      password: configService.get('password'),
      database: configService.get('database'),
      entities: [__dirname + '/src/**/*.entity{.ts,.js}'],
      migrations: [__dirname + '/src/migrations/*{.ts,.js}'],
      cli: {
        migrationsDir: 'src/migrations',
      },
      synchronize: false,
      migrationsRun: true,
      logging: true,
      logger: 'file',
    };
  }
}

export const typeOrmConfigAsync: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  useFactory: async (configService: ConfigService): Promise<TypeOrmModuleOptions> => TypeOrmConfig.getOrmConfig(configService),
  inject: [ConfigService]
};