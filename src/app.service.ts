import { Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { Seed } from './seed.class';
import { Users } from './users/users.entity';
import { Files } from './files/files.entity';
import { Posts } from './posts/posts.entity';

@Injectable()
export class AppService extends Seed {
  constructor (entityManager: EntityManager) {
    super(entityManager);
    this.fakeData();
  }

  private async fakeData(): Promise<void> {
    this.fakeIt(Users);
    this.fakeIt(Posts);
    this.fakeIt(Files);
   }


  getHello(): string {
    return 'Hello World!';
  }
}
