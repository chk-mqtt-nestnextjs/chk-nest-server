import { Posts } from 'src/posts/posts.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Unique,
  OneToMany
} from 'typeorm';


export enum UsersRole {
    ADMIN = 'admin',
    VISITOR = 'visitor',
  }

// export enum Roles {
//   user = 'user',
//   admin = 'admin',
// }

@Unique('my_unique_constraint', ['email']) // make firstName unique
@Entity("users")
export class Users {
  @PrimaryGeneratedColumn() id: number;
  @Column({ unique: true }) email: String;
  @Column({  }) password: String;
  @Column({ default: true }) isActive?: Boolean;
  @Column({ type: 'enum', enum: UsersRole, default: UsersRole.ADMIN })
  role: UsersRole;
  @Column({ type: 'varchar', length: 500 })
  salt: string;
  // @OneToOne(() => Role)
  // @JoinColumn()
  // role: Role;

  // @OneToMany(
  //   type => Posts,
  //   (post: Posts) => post.user,
  //   { onUpdate: 'CASCADE', onDelete: 'CASCADE' },
  // )
  // posts: Posts[];
  
  constructor(users?: Partial<Users>) {
    Object.assign(this, users);
  }
}
