import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from './users.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
  ) {}

  async create(data: any) {
    const newUser =  this.usersRepository.save(data);
    return newUser;
  }

  async findOne(condition: any) {
    return this.usersRepository.findOne(condition);
  }
}
