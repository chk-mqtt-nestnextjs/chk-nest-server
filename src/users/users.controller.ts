import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UnauthorizedException,
} from '@nestjs/common';
// import { JwtService } from '@nestjs/jwt';
import { Response, Request } from 'express';
import * as bcrypt from 'bcrypt';
import { genSalt } from 'bcryptjs';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    // private jwtService: JwtService,
  ) {

  }
  // @Post('register')
  // async register(
  //   @Body('email') email: string,
  //   @Body('password') password: string,
  //   @Res() response: Response,
  // ) {
  //   const salt = await genSalt(10);
  //   const hashedPassword = await bcrypt.hash(password, salt);
  //   try {
  //     const user = await this.usersService.create({
  //       email,
  //       password: hashedPassword,
  //       permission: 2,
  //     });
  //     delete user.password;
  //     response.status(200).send(user);
  //   } catch (err) {
  //     response.status(500).send(err);
  //   }
  // }

  //todo bcrypt.compare hash
  // @Post('login')
  // async login(
  //   @Body('email') email: string,
  //   @Body('password') password: string,
  //   @Res({ passthrough: true }) response: Response,
  // ) {
  //   try {
  //     console.log('debug 1');
  //     const user = await this.usersService.findOne({ email });
  //     console.log('debug 1', user);
  //     console.log('debug 1.1', bcrypt.compare(password, user.salt));
  //     if (!user) {
  //       throw new BadRequestException('invalid credentials');
  //     }

  //     if (!(await bcrypt.compare(password, user.salt))) {
  //       throw new BadRequestException('invalid credentials');
  //     }

  //     const jwt = await this.jwtService.signAsync({ id: user.id });
  //     console.log('debug 2');
  //     // response.cookie('jwt', jwt, { httpOnly: true });

  //     delete user.password;
  //     response.status(200).send({
  //       jwt: jwt,
  //       user
  //     });
  //   } catch (err) {
  //     response.status(500).send(err);
  //   }
  // }

  // @Get('user')
  // async user(@Req() request: Request,  @Res() response: Response,) {
  //   try {
  //     const auth = request.headers['authorization'];
  //     const data = await this.jwtService.verifyAsync(auth);

  //     if (!data) {
  //       throw new UnauthorizedException();
  //     }

  //     const user = await this.usersService.findOne({ id: data['id'] });

  //     const { password, ...result } = user;

  //     response.status(200).send(result);
  //   } catch (e) {
  //     // throw new UnauthorizedException();
  //     response.status(500).send(new UnauthorizedException());
  //   }
  // }

  @Post('logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');

    return {
      message: 'success',
    };
  }
}


