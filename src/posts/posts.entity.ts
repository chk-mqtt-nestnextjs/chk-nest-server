import { Files } from 'src/files/files.entity';
import { Users } from 'src/users/users.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  OneToOne
} from 'typeorm';


@Entity("posts")
export class Posts {
  @PrimaryGeneratedColumn() id: number;
  @Column() title: String;
  @Column({ type: 'text'}) body: String;
  @Column({ type: "integer" })
  usersId: number;
  @Column({ type: "integer" })
  filesId: number;

  @ManyToOne(type => Users , users => users.id, { nullable: false, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({name: "usersId"})   
  user: Users;

  @OneToOne(type => Files , files => files.id, { nullable: true, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({name: "filesId"})   
  files: Files;

  constructor(posts?: Partial<Posts>) {
    Object.assign(this, posts);
  }
}
