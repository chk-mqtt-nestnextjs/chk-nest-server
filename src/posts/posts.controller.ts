import {
  Body,
  Controller,
  Post,
  Request,
  Response,
  Get,
  UseGuards,
  Put,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

export interface PostsResponse {
  id: number;
  body: string;
  title: string;
  user: User;
}

export interface User {
  email: string;
  id: number;
  isActive: boolean;
  role: string;
}

@Controller('posts')
export class PostsController {
  constructor(
    private readonly postsService: PostsService, // private jwtService: JwtService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getOne(@Request() req: any, @Response() res: any) {
    const { id } = req.params;
    this.postsService.findOne(id).subscribe(
      (post) => {
        res.status(200).send(post);
      },
      (err) => {
        res.status(500).send(err);
      },
    );

    // try {
    //   this.postsService.findOne(id).subscribe(post => {
    //     console.log(post);
    //     res.status(200).send(post);
    //   });
    // } catch (err) {
    //   res.status(500).send(err);
    // }
  }

  @Get()
  get(@Request() req: any, @Response() res: any) {
    this.postsService.findAll().subscribe(
      (post) => {
        return res.status(200).send(post);
      },
      (err) => {
        return res.status(500).send(err);
      },
    );

    // try {
    //   const post = await this.postsService.findAll();
    //   return res.status(200).send(post);
    // } catch (err) {
    //   return res.status(500).send(err);
    // }
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @Body('title') title: string,
    @Body('body') body: string,
    @Body('usersId') usersId: string,
    @Body('filesId') filesId: string,
    @Response() res: any,
  ) {
    this.postsService
      .create({
        title,
        body,
        usersId,
        filesId,
      })
      .subscribe(
        (post) => {
          res.status(200).send(post);
        },
        (err) => {
          res.status(500).send(err);
        },
      );

    // try {
    //   const post = await this.postsService.create({
    //     title,
    //     body,
    //     usersId,
    //     filesId,
    //   });
    //   res.status(200).send(post);
    // } catch (err) {
    //   res.status(500).send(err);
    // }
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async update(
    @Body('title') title: string,
    @Body('body') body: string,
    @Body('usersId') usersId: string,
    @Body('filesId') filesId: string,
    @Response() res: any,
    @Request() req: any,
  ) {
    const { id } = req.params;
    this.postsService
      .update(id, {
        title,
        body,
        usersId,
        filesId,
      })
      .subscribe(
        (post) => {
          res.status(200).send(post);
        },
        (err) => {
          res.status(500).send(err);
        },
      );
      
    // try {
    //   const post = await this.postsService.create({
    //     title,
    //     body,
    //     usersId,
    //     filesId,
    //   });
    //   res.status(200).send(post);
    // } catch (err) {
    //   res.status(500).send(err);
    // }
  }
}
