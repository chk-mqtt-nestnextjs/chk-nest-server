import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Repository } from 'typeorm';
import { Posts } from './posts.entity';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Posts)
    private postsRepository: Repository<Posts>,
  ) {}

  create(data): Observable<object> {
    return from(this.postsRepository.save(data));
  }

  update(id, data): Observable<object> {
    return from(this.postsRepository.update(id, data));
  }

  findAll(): Observable<object> {
    return from(this.postsRepository.find({ relations: ['user','files'] }));
  }

  findOne(id): Observable<object> {
    return from(this.postsRepository.findByIds(id, { relations: ['user'] }));
    // return from(this.postsRepository.findByIds(id ,{relations: ['user']})).pipe(
    //   map((post: PostsResponse) => post));
  }
}
