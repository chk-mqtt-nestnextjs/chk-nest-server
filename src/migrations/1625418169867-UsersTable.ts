import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";
import { UsersRole } from "../users/users.entity";

export class UsersTable1625418169867 implements MigrationInterface {
    name = 'UsersTable1625418169867'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
          new Table({
            name: 'users',
            columns: [
              {
                name: 'id',
                type: 'int4',
                isPrimary: true,
                isGenerated: true,
                generationStrategy: 'increment',
              },
              {
                name: 'email',
                type: 'varchar',
                isNullable: false,
              },
              {
                name: 'password',
                type: 'varchar',
                isNullable: false,
              },
              {
                name: 'salt',
                type: 'varchar',
                isNullable: false,
              },
              {
                name: 'isActive',
                type: 'boolean',
                isNullable: false,
                default: true,
              },
              {
                name: 'role',
                type: 'enum',
                enum: [UsersRole.ADMIN, UsersRole.VISITOR],
              }
              // {
              //   name: 'roleId',
              //   type: 'int4'
              // },
            ],
          }),
          true,
        );
    
      //   const foreignKey = new TableForeignKey({
      //     columnNames: ["roleId"],
      //     referencedColumnNames: ["id"],
      //     referencedTableName: "role",
      //     onDelete: "CASCADE"
      // });
      // await queryRunner.createForeignKey("users", foreignKey);
      }
    
      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('users');
      }

}
