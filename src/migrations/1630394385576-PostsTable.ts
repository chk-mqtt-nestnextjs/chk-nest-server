import { type } from 'node:os';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableColumn,
  TableForeignKey,
} from 'typeorm';

export class PostsTable1630394385576 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'posts',
        columns: [
          {
            name: 'id',
            type: 'int4',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'title',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'body',
            type: 'text',
            isNullable: false,
          },
          {
            name: 'usersId',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'filesId',
            type: 'int',
            isNullable: true
          },
        ],
      }),
      true,
    );
    queryRunner.clearSqlMemory();

    const userForeignKey = new TableForeignKey({
      name: 'userForeignKey',
      columnNames: ['usersId'],
      referencedColumnNames: ['id'],
      referencedTableName: 'users',
      onDelete: 'CASCADE'
    });

    const filesForeignKey = new TableForeignKey({
        name: 'filesForeignKey',
        columnNames: ['filesId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'files',
        onDelete: 'CASCADE'
      });
    
    await queryRunner.createForeignKey('posts', userForeignKey);

   
    await queryRunner.createTable(
      new Table({
        name: 'files',
        columns: [
          {
            name: 'id',
            type: 'int4',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'size',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'mimetype',
            type: 'varchar'
          },
          {
            name: 'destination',
            type: 'varchar'
          },
          {
            name: 'path',
            type: 'varchar'
          },
          {
            name: 'usersId',
            type: 'int',
          }
        ],
      }),
      true,
    );

    queryRunner.clearSqlMemory();

    const userFileForeignKey = new TableForeignKey({
      name: 'userFileForeignKey',
      columnNames: ['usersId'],
      referencedColumnNames: ['id'],
      referencedTableName: 'users',
      onDelete: 'CASCADE',
    });
    await queryRunner.createForeignKey('files', userFileForeignKey);

    // const postFileForeignKey = new TableForeignKey({
    //   name: 'postFileForeignKey',
    //   columnNames: ['postsId'],
    //   referencedColumnNames: ['id'],
    //   referencedTableName: 'posts',
    //   onDelete: 'CASCADE',
    // });
    // await queryRunner.createForeignKey('files', postFileForeignKey);
    await queryRunner.createForeignKey('posts', filesForeignKey);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    // const table = await queryRunner.getTable("users");
    // const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("usersId") !== -1);
    // await queryRunner.dropForeignKey("posts", foreignKey);
    // await queryRunner.query("ALTER TABLE `posts` DROP FOREIGN KEY `usersId`");
    // await queryRunner.query("ALTER TABLE `posts` DROP FOREIGN KEY `filesId`");
    // await queryRunner.query("ALTER TABLE `files` DROP FOREIGN KEY `usersId`");
    // await queryRunner.query("ALTER TABLE `files` DROP FOREIGN KEY `postsId`");
   
    await queryRunner.dropForeignKey('posts', 'userForeignKey');
    await queryRunner.dropForeignKey('posts', 'filesForeignKey');
    await queryRunner.dropForeignKey('files', 'userFileForeignKey');
    // await queryRunner.dropForeignKey('files', 'postFileForeignKey');
    await queryRunner.dropTable('files');
    await queryRunner.dropTable('posts');
  }
}
