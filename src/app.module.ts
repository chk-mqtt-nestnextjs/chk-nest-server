import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// import * as ormconfig from '../ormconfig';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from '../config';
import { typeOrmConfigAsync } from '../typeorm.config';
import { PostsService } from './posts/posts.service';
import { PostsModule } from './posts/posts.module';
import { FilesController } from './files/files.controller';
import { FilesModule } from './files/files.module';

const ENV = process.env.NODE_ENV;
console.log('ENV', ENV);
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: !ENV ? '.env' : `.env.production`,
      load: [configuration],
      isGlobal: true
    }),
    // TypeOrmModule.forRoot(ormconfig),
    TypeOrmModule.forRootAsync(typeOrmConfigAsync),
    UsersModule,
    AuthModule,
    PostsModule,
    FilesModule
  ],
  controllers: [AppController, FilesController],
  providers: [AppService],
})
export class AppModule {}
