import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Files } from './files.entity';
import { FilesService } from './files.service';

@Module({
  imports:[
    TypeOrmModule.forFeature([Files]),
  ],
  exports: [FilesService],
  providers: [FilesService]
})
export class FilesModule {}
