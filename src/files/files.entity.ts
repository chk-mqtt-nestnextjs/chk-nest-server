import { Posts } from 'src/posts/posts.entity';
import { Users } from 'src/users/users.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';


@Entity("files")
export class Files {
  @PrimaryGeneratedColumn() id: number;
  @Column() name: String;
  @Column({ type: 'int'}) size: number;
  @Column() mimetype: String;
  @Column() destination: String;
  @Column() path: String;
  @Column({ type: "integer" })
  usersId: number;
  @ManyToOne(
    () => Users,
    { onUpdate: 'CASCADE', onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'usersId' })
  user: Users;

  // @ManyToOne(
  //   () => Posts,
  //   { onUpdate: 'CASCADE', onDelete: 'CASCADE' },
  // )
  // @JoinColumn({ name: 'postsId' })
  // post: Posts;


  constructor(files?: Partial<Files>) {
    Object.assign(this, files);
  }
}
