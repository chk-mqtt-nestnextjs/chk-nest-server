import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from } from 'rxjs';
import { Repository } from 'typeorm';
import { Files } from './files.entity';

@Injectable()
export class FilesService {
  constructor(
    @InjectRepository(Files)
    private filesRepository: Repository<Files>,
  ) {}

  find(id) {
    return from(this.filesRepository.findByIds(id));
  }

  create(data) {
    return from(this.filesRepository.save(data));
  }
}
