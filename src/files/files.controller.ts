import {
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
  Request,
  Response,
  Get,
  UseGuards,
  Param,
  Body,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import path = require('path');
import { v4 as uuidv4 } from 'uuid';
import { diskStorage } from 'multer';
import { FilesService } from './files.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { from, Observable, of } from 'rxjs';
import { join } from 'path';

export const storage = {
  storage: diskStorage({
    destination: './uploads/posts',
    filename: (req, file, cb) => {
      const filename: string =
        path.parse(file.originalname).name.replace(/\s/g, '') + uuidv4();
      const extension: string = path.parse(file.originalname).ext;
      cb(null, `${filename}${extension}`);
    },
  }),
};

@Controller('files')
export class FilesController {
  constructor(private readonly filesService: FilesService) {}

  @Get('image/:name')
  findImage(
    @Param('name') imagename,
    @Response() res: any,
  ): Observable<Object> {
    return of(res.sendFile(join(process.cwd(), 'uploads/posts/' + imagename)));
  }

  @UseGuards(JwtAuthGuard)
  @Get(':filesId')
  find(@Request() req: any, @Response() res: any) {
    const { filesId } = req.params;
    this.filesService.find(filesId).subscribe(
      (file) => {
        res.status(200).send(file);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
    // return from(this.filesService.find(filesId));
  }

  @UseGuards(JwtAuthGuard)
  @Post('image/upload')
  @UseInterceptors(FileInterceptor('file', storage))
  uploadFile(@UploadedFile() file, @Request() req: any, @Response() res: any) {
    const { userId } = req.body;
    const { mimetype, destination, filename, path, size } = file;

    const data = {
      name: filename,
      size: size,
      mimetype: mimetype,
      destination: destination,
      path: path,
      usersId: userId,
    };

    this.filesService.create(data).subscribe(
      (file) => {
        res.status(200).send(file);
      },
      (err) => {
        res.status(500).send(err);
      },
    );
  }
}
