import { Controller, Request, Response, Get, Header, Post, UseGuards, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { Users, UsersRole } from './users/users.entity';
import { genSalt, hashSync } from 'bcryptjs';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private readonly authService: AuthService) {}

  // @UseGuards(LocalAuthGuard)

  @Get('auth/check')
  async userInfo(@Request() req: any, @Response() res: any) {
    console.log('auth/info');
    const auth = req.headers['authorization'];
    if(auth) {
      const validateRes: any = this.authService.validateUser(null, null, auth);
      validateRes.then(async (result) => {
        res.send(200, result);
      }).catch(e => {
        res.send(500, e);
      });
    } 
  }

  @Post('auth/login')
  async login(@Request() req: any, @Response() res: any) {
    const {email, password} = req.body;
    const validateRes: any = this.authService.validateUser(email, password, null);
    validateRes.then(async (result) => {
      if(result) {
        const userJwt = await this.authService.login(result);
        res.send(200, userJwt)
      }else {
        res.send(500, 'user account and password not correct')
      }
    });
    
  }

  @Post('auth/register')
  async register(@Body() body: Partial<Users>) {
    console.log('debug register');
    try {
      const salt = await genSalt(10);
      const { email ,password, ...reset } = body;
      const u: Partial<Users> = {
        salt,
        ...reset,
        email,
        password: hashSync(password, salt),
        role: UsersRole.VISITOR,
      };
      const user = await this.authService.register(u);
      delete user.password;
      delete user.salt;
      return user;
    } catch (error) {
      throw error;
    }
  }
  
  @Get()
  @Header('Content-Type', 'text/html')
  getHello(): { name: string } {
    return { name: 'Max' };
  }
}
