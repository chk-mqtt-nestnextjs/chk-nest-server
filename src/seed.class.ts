import { EntityManager } from 'typeorm';
import { Users, UsersRole } from './users/users.entity';
import { internet } from 'faker';
import { genSalt, hashSync } from 'bcryptjs';
import { Files } from './files/files.entity';
import { Posts } from './posts/posts.entity';

export class Seed {
  private users: Array<Partial<Users>>;
  private posts: Array<Partial<Posts>>;
  private files: Array<Partial<Files>>;
  private salt: string;
  constructor(private readonly entityManager: EntityManager) {
    this.users = [];
    this.posts = [];
    this.files = [];
  }

  private async addData<T>(
    data: Array<Partial<T>>,
    entity: any,
    fun?: (savedData: Array<Partial<T>>) => void,
  ): Promise<void> {
    return this.entityManager
      .save<T, T>(entity, data as any)
      .then((savedData: Array<Partial<T>>) => {
        if (fun) {
          fun(savedData);
        }
      })
      .catch(console.error);
  }

  async fakeIt<T>(entity: any): Promise<void> {
    this.salt = await genSalt(10);
    const data = await this.entityManager.find(entity);
    if (data?.length <= 0) {
      switch (entity) {
        case Users:
          return this.addData(
            this.userData(),
            entity,
            (savedData: Array<Partial<Users>>) => (this.users = savedData),
          );
          break;
          case Posts:
            return this.addData(
              this.postData(),
              entity,
              (savedData: Array<Partial<Posts>>) => (this.posts = savedData),
            );
            break;
          case Files:
            return this.addData(
              this.fileData(),
              entity,
              (savedData: Array<Partial<Files>>) => (this.files = savedData),
            );
           break;  
        default:
          break;
      }
    }
  }

  private userData(): Array<Partial<Users>> {
    return Array.from({ length: 1 }).map<Partial<Users>>(() => {
      return {
        email: 'asd@asd.com', // internet.email()
        role: UsersRole.ADMIN,
        password: hashSync('asdasd', this.salt),
        salt: this.salt,
      };
    });
  }

  private postData(): Array<Partial<Posts>> {
    const posts: Array<any> = [
      {
        title: "WHAT'S NEW",
        body: `<h1>WHAT'S NEW</h1><h4>Stack Overflow Survey Reconfirms Developers Love Docker</h4><p>Docker is the #1 most wanted and #2 most loved developer tool, and helps millions of developers build, share and run any app, anywhere - on-prem or in the cloud.</p><p><a href="https://insights.stackoverflow.com/survey/2021#most-loved-dreaded-and-wanted-tools-tech-want" rel="noopener noreferrer" target="_blank" style="background-color: rgb(255, 255, 255); color: rgb(36, 150, 237);">Learn more!</a></p>`,
        usersId: 1,
        filesId: 1
      },
      {
        title: "Use containers to Build, Share and Run your applications",
        body: `<h2>Package Software into Standardized Units for Development, Shipment and Deployment</h2><p>A container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another. A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings.</p><p>Container images become containers at runtime and in the case of Docker containers - images become containers when they run on&nbsp;<a href="https://www.docker.com/products/container-runtime" rel="noopener noreferrer" target="_blank" style="color: rgb(36, 150, 237);">Docker Engine</a>. Available for both Linux and Windows-based applications, containerized software will always run the same, regardless of the infrastructure. Containers isolate software from its environment and ensure that it works uniformly despite differences for instance between development and staging.</p><p>Docker containers that run on Docker Engine:</p><ul><li><strong>Standard:</strong>&nbsp;Docker created the industry standard for containers, so they could be portable anywhere</li><li><strong>Lightweight:</strong>&nbsp;Containers share the machine’s OS system kernel and therefore do not require an OS per application, driving higher server efficiencies and reducing server and licensing costs</li><li><strong>Secure:</strong>&nbsp;Applications are safer in containers and Docker provides the strongest default isolation capabilities in the industry</li></ul><p><br></p>`,
        usersId: 1,
        filesId: 1
      },
      {
        title: "Developing with Docker",
        body: `<h2 class="ql-align-center">Developing with Docker</h2><h3 class="ql-align-center">Developing apps today requires so much more than writing code. Multiple languages, frameworks, architectures, and discontinuous interfaces between tools for each lifecycle stage creates enormous complexity. Docker simplifies and accelerates your workflow, while giving developers the freedom to innovate with their choice of tools, application stacks, and deployment environments for each project.</h3><p><br></p>`,
        usersId: 1,
        filesId: 1
      },
      {
        title: "HARBETH P3ESR 書架型喇叭",
        body: `<ul><li>The gold standard in mini-monitors.</li><li><br></li><li>The mini but mighty P3ESR speaker and brand flagship, will be available in a striking olive wood veneer in 2018, with the additional added features of Harbeth’s 40th Anniversary range including; the latest WBT-nextgen binding posts, British-made audio grade poly capacitors, and Harbeth’s 40th anniversary ultra-pure OFC internal cable. Each speaker will also include the Harbeth 40th Anniversary limited edition front and back badges and the metallic black and gold anniversary grille badge.</li><li>Harbeth’s flagship model, the M40.2, will also available in this stunning limited edition veneer as part of our momentous 40th anniversary.</li></ul><h3>Finishes</h3><ul><li>Our cabinets are handmade and feature the finest veneers. Please note that veneers vary in both colour, shade and grain structure, particularly our eucalyptus veneer.</li><li><br></li><li>The P3ESR 40th Anniversary Edition is available in Olive Wood.</li><li><br></li><li><br></li><li>Harbeth P3ESR四十週年紀念版主要性能規格</li><li class="ql-indent-1">形式：兩音路兩單體密閉式書架喇叭（外觀為Olive Wood橄欖木皮）</li><li class="ql-indent-1">單體：19mm軟半球振膜高音一支，Harbeth RADIAL2聚合物110mm振膜中低音一支</li><li class="ql-indent-1">頻率響應：75Hz-20kHz±3dB</li><li class="ql-indent-1">阻抗：6歐姆效率：83.5dB/1W/1m</li><li class="ql-indent-1">建議擴大機功率：每聲道15瓦起</li><li class="ql-indent-1">承受功率：50瓦</li><li class="ql-indent-1">尺寸（高×寬×深，mm）：306×190×184（不含喇叭端子突出物）</li><li class="ql-indent-1">重量：6.1公斤</li></ul><p><br></p>`,
        usersId: 1,
        filesId: 2
      },
      {
        title: "TFK ECC82",
        body: `<p><strong style="color: rgb(0, 0, 0);">TFK ECC82</strong></p><p><span style="color: rgb(0, 0, 0);"><img src="http://saabclubtaiwan.netfirms.com/Netphoto/nfpicturepro/albums/userpics/10027/TFK%20ECC82-1.jpg"></span></p><p><br></p><p><span style="color: rgb(0, 0, 0);">首先登場的是TFKECC82, 這一對並不是TFK最好的管子, 上面還有E82CC, ECC802S等, 但這一對的優點在於數值的左右完全相同( 從幾十對中挑出來的 ), 以這為基準相互比較( 所以都給80分)</span></p><p><span style="color: rgb(0, 0, 0);">80 S: 弦樂的厚度極為優秀, 低音的量感比較多, 不但多而且很輕鬆, 木管出現時那個氣流的感覺很強烈, 但在齊奏時, 中低音的部份缺乏那一種很勁, 仍是很迷人的聲音.</span></p><p><span style="color: rgb(0, 0, 0);">80 B: 好厚的提琴聲, 但高音的延伸不佳, 很和順溫暖的聲音, 中高音很厚但是往上的延伸不足, 到了E弦時應該出現華麗的光澤並沒有看到, 這樣的弦樂只能說不刺耳, 但也不會有激情.</span></p><p><span style="color: rgb(0, 0, 0);">80 P:中高音的表現滑順, 中音也極厚, 非常成功得把男中音的厚度表現得淋漓盡至, 高音轉折一點都不留痕跡, 不知不覺中, 輕舟已過萬重山.</span></p><p><span style="color: rgb(0, 0, 0);">80 F:女聲的中音厚度不錯, 但聽來細節似乎少了一點, 女聲的年齡也似乎老了一點, 如果廳哦是蔡琴應該會表現得更好, 鋼琴頗有木味, 但也少了那一點刺激的感覺.</span></p>`,
        usersId: 1,
        filesId: 3
      },
      {
        title: "TEST",
        body: `<p>test</p>`,
        usersId: 1,
        filesId: 2
      }
    ];

    return posts.map<any>((data) => {
      return {
        title: data.title,
        body: data.body,
        usersId: data.usersId,
        filesId: data.filesId
      };
    });
  }

  private fileData(): Array<Partial<Files>> {
    const files = [
      {
        name: "cgroups-dockerb7658e9e-2769-4f02-89e3-0fba30b3257a.jpg",
        size: 34892,
        mimetype: "image/jpeg",
        destination: "./uploads/posts",
        path: "uploads\posts\cgroups-dockerb7658e9e-2769-4f02-89e3-0fba30b3257a.jpg",
        usersId: 1,
      },
      {
        name: "Harbeth-M20.1-1f41017c9-3477-44d1-bca8-3f981cfb44fc.jpg",
        size: 75038,
        mimetype: "image/jpeg",
        destination: "./uploads/posts",
        path: "uploads\posts\Harbeth-M20.1-1f41017c9-3477-44d1-bca8-3f981cfb44fc.jpg",
        usersId: 1,
      },
      {
        name: "b86f8010-55e9-4335-9f54-dfbce228428bc87d7327-ccd8-4c4e-9ef2-e87ce50faea0.jpg",
        size: 2775191,
        mimetype: "image/jpeg",
        destination: "./uploads/posts",
        path: "uploads\posts\b86f8010-55e9-4335-9f54-dfbce228428bc87d7327-ccd8-4c4e-9ef2-e87ce50faea0.jpg",
        usersId: 1,
      }
    ];

    return files.map<any>((data) => {
      return {
        name: data.name,
        size: data.size,
        mimetype: data.mimetype,
        destination: data.destination,
        path: data.path,
        usersId: data.usersId,
      };
    });
  }
}
