import { BadRequestException, CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
// import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';

@Injectable()
export class JwtAuthGuard implements CanActivate {
    constructor(public authService: AuthService) { 
        // super() // 實作 CanActivate 不需要
    }

    // getRequest(context: ExecutionContext) {
    //     const ctx = GqlExecutionContext.create(context);
    //     return ctx.getContext().req;
    // }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        // const req = this.getRequest(context);
        const req = context.switchToHttp().getRequest();
        const authHeader = req.headers.authorization as string;

        if (!authHeader) {
            throw new BadRequestException('Authorization header not found.');
        }
        
        const { id } = await this.authService.validateUser(null, null, authHeader)
        if (id) {
            req.id = id;
            return true;
        }
        throw new UnauthorizedException('Token not valid');
    }
}
