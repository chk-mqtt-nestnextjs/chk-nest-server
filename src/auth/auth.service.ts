import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Users } from 'src/users/users.entity';
import { UsersService } from 'src/users/users.service';
import { hashSync } from 'bcryptjs';
import { rejects } from 'node:assert';

@Injectable()
export class AuthService {
  constructor(
    public readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(
    email?: string,
    pass?: string,
    jwt?: string,
  ): Promise<Partial<Users>> {
    console.log('debug');
    if (jwt !== null) {
      console.log('jwt ', jwt);
      const data = await this.jwtService.verifyAsync(jwt);
      console.log(data);
      if (!data) {
        return null;
      }
      const user = await this.usersService.findOne({ id: data['id']});
      console.log(user);
      delete user.salt;
      delete user.password;
      const { salt, password, ...result } = user;
      return result;
    } else {
      const user = await this.usersService.findOne({ email });
      console.log('debug 2 ', user);
      if (user) {
        const hash: any = hashSync(pass, user.salt);
        console.log(hash);
        if (user && user.password === hash) {
          delete user.salt;
          delete user.password;
          const { salt, password, ...u } = user;
          return u;
        }
        return null;
      }else {
        Promise.reject('User info not found');
      }
    }
  }
  // $2a$10$l.7o/O35p3E53z1izcIr2Ozig/q9qMHlKvW2oeUlLwKYnoNqU8ECq
  // $2a$10$niLDEKE.VCAr4a.FWMxuLerUjktAX2POcKnsHfG4Ckm06/mEFfrCK
  // $2a$10$niLDEKE.VCAr4a.FWMxuLerUjktAX2POcKnsHfG4Ckm06/mEFfrCK
  // $2a$10$niLDEKE.VCAr4a.FWMxuLe

  async login(user: Partial<Users>) {
    console.log('debug user ', user);
    const jwt = await this.jwtService.signAsync({
      email: user.email,
      id: user.id,
      role: user.role,
    });
    console.log('debug jwt ', jwt);
    return {
      ...user,
      jwt,
    };
  }

  async register(user: Partial<Users>): Promise<Partial<Users>> {
    try {
      const s = await this.usersService.create(user);
      return s;
    } catch (error) {
      return error;
    }
  }
}
